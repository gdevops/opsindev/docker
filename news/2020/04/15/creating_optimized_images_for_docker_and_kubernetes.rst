
.. index::
   pair: Jérôme Petazzoni ; 2020-04-15
   pair: Docker; Optimized Images for Docker and Kubernetes

.. _jpetazzo_2020_04_15:

====================================================================================================
Video **Creating Optimized Images for Docker and Kubernetes** with **nixery** by Jérôme Petazzoni
====================================================================================================

.. seealso::

   - https://www.youtube.com/watch?v=UbXv-T4IUXk
   - :ref:`jpetazzo_2020_04_07`
   - https://nixery.dev/
   - https://enix.io/fr/blog/cherie-j-ai-retreci-docker-part3/
   - https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html
   - https://x.com/nixos_org
   - :ref:`docker_jpetazzo`
   - https://fr.slideshare.net/datakurre/nix-for-python-developers
   - https://github.com/NixOS
   - https://github.com/NixOS/nix
   - https://github.com/NixOS/nixpkgs
   - https://github.com/NixOS/nixos-weekly
   - https://github.com/NixOS/nixos-weekly/pulls
   - https://github.com/google/nixery
   - :ref:`kub_jpetazzo_2020_04_15`



.. figure:: video.png
   :align: center

   https://www.youtube.com/watch?v=UbXv-T4IUXk


.. figure:: dockerfile.png
   :align: center

   https://enix.io/fr/blog/cherie-j-ai-retreci-docker-part3/


:source: https://enix.io/fr/blog/cherie-j-ai-retreci-docker-part3/
:source: https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html
