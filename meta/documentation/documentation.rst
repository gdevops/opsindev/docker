
.. _tuto_docker_doc_meta_infos:

=====================
Meta doc
=====================




Head project : tuto_devops
=============================

.. seealso::

   - https://gdevops.frama.io/opsindev/tutorial/


Inspiration : sphinx-blogging de Chris Holdgraf
========================================================

.. seealso::

   - https://predictablynoisy.com/posts/2020/sphinx-blogging/
   - https://gdevops.gitlab.io/tuto_ablog/posts/2020/10/10/sphinx_blogging.html
   - https://gdevops.gitlab.io/tuto_ablog/
   - https://gdevops.gitlab.io/tuto_documentation/



Gitlab project
=================

.. seealso::

   - https://framagit.org/gdevops/opsindev/tuto_docker


Issues
---------

.. seealso::

   - https://framagit.org/gdevops/opsindev/tuto_docker/-/boards


Pipelines
--------------

.. seealso::

   - https://framagit.org/gdevops/opsindev/tuto_docker/-/pipelines


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    import sphinx
    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2019-{now.year}, {author} Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"




root directory
===============

::

    $ ls -als

::


    4 drwxr-xr-x 13   4096 nov.   4 13:49 .
    4 drwxr-xr-x 27   4096 nov.   1 14:48 ..
    4 drwxr-xr-x  4   4096 nov.   4 13:40 _build
    4 drwxr-xr-x  2   4096 mars   6  2020 commands
    4 -rw-r--r--  1   3899 nov.   4 13:43 conf.py
    4 drwxr-xr-x 24   4096 nov.   4 13:48 documentation
    4 -rw-r--r--  1     99 nov.   4 13:29 feed.xml
    4 drwxr-xr-x  8   4096 nov.   4 13:49 .git
    4 -rw-r--r--  1    355 mars   6  2020 .gitattributes
    4 -rw-r--r--  1    129 mars   6  2020 .gitignore
    4 -rw-r--r--  1    211 oct.  11 08:42 .gitlab-ci.yml
    4 drwxr-xr-x  2   4096 mars   6  2020 glossaire
    4 drwxr-xr-x  2   4096 mars   6  2020 images
    4 drwxr-xr-x  2   4096 oct.  19 08:05 index
    4 -rw-r--r--  1   1324 nov.   4 13:49 index.rst
    4 drwxr-xr-x  2   4096 mars   6  2020 installation
    4 -rw-r--r--  1    787 mars   6  2020 make.bat
    4 -rw-r--r--  1   1154 mars  30  2020 Makefile
    4 drwxr-xr-x  3   4096 oct.  27 16:35 meta
    4 drwxr-xr-x  6   4096 nov.   4 13:41 news
    88 -rw-r--r--  1  88534 nov.   4 13:38 poetry.lock
    4 -rw-r--r--  1   1345 oct.  26 07:28 .pre-commit-config.yaml
    4 -rw-r--r--  1    498 oct.  27 16:30 pyproject.toml
    4 -rw-r--r--  1    470 mars   6  2020 README.md
    4 -rw-r--r--  1   2135 nov.   4 13:39 requirements.txt
    4 drwxr-xr-x  2   4096 mars   6  2020 _static




pyproject.toml
=================

.. literalinclude:: ../../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../../Makefile
   :linenos:
