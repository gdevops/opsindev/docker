.. index::
   ! compose-file
   pair: compose-file ; 3.6

.. _compose_file:

==============================
**compose-file**
==============================

- https://docs.docker.com/compose/compose-file/

.. toctree::
   :maxdepth: 3

   specification/specification
   versions/versions
