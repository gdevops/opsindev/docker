.. index::
   pair: compose-file ; Specification

.. _compose_file_spec:

=================================
**compose-file specification**
=================================

- https://github.com/compose-spec/compose-spec/blob/master/spec.md


Introduction
==============


The Compose file is a YAML file defining services, networks, and volumes
for a Docker application.

The latest and recommended version of the Compose file format is defined
by the Compose Specification.

The Compose spec merges the legacy 2.x and 3.x versions, aggregating
properties across these formats and is implemented by :ref:`Compose 1.27.0+ <docker_compose_1_27>`.

Status of this document
==========================

This document specifies the Compose file format used to define multi-containers
applications. Distribution of this document is unlimited.

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”,
“SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this
document are to be interpreted as described in RFC 2119.


The Compose application model
=================================

- https://docs.docker.com/compose/compose-file/#the-compose-application-model

**The Compose specification allows one to define a platform-agnostic container
based application**.

Such an application is designed as a set of containers  which have to both
run together with adequate shared resources and communication channels.

Computing components of an application are defined as Services.

A Service is an abstract concept implemented on platforms by running the
same container image (and configuration) one or more times.

Services communicate with each other through Networks.
In this specification, a Network is a platform capability abstraction to
establish an IP route between containers within services connected together.
Low-level, platform-specific networking options are grouped into the Network
definition and MAY be partially implemented on some platforms.

Services store and share persistent data into Volumes.
The specification describes such a persistent data as a high-level
filesystem mount with global options. Actual platform-specific
implementation details are grouped into the Volumes definition
and MAY be partially implemented on some platforms.

Some services require configuration data that is dependent on the runtime
or platform. For this, the specification defines a dedicated concept:
Configs. From a Service container point of view, Configs are comparable
to Volumes, in that they are files mounted into the container.

But the actual definition involves distinct platform resources and
services, which are abstracted by this type.

A Secret is a specific flavor of configuration data for sensitive data
that SHOULD NOT be exposed without security considerations.
Secrets are made available to services as files mounted into their
containers, but the platform-specific resources to provide sensitive
data are specific enough to deserve a distinct concept and definition
within the Compose specification.

Distinction within Volumes, Configs and Secret allows implementations
to offer a comparable abstraction at service level, but cover the
specific configuration of adequate platform resources for well identified
data usages.

A Project is an individual deployment of an application specification
on a platform.
A project’s name is used to group resources together and isolate them
from other applications or other installation of the same Compose
specified application with distinct parameters.

A Compose implementation creating resources on a platform MUST prefix
resource names by project and set the label com.docker.compose.project.

Project name can be set explicitly by top-level name attribute.
Compose implementation MUST offer a way for user to set a custom project
name and override this name, so that the same compose.yaml file can be
deployed twice on the same infrastructure, without changes, by just
passing a distinct name.
