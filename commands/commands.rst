

.. index::
   pair: docker ; commands
   pair: docker ; attach
   pair: docker ; build
   pair: docker ; build --no-cache
   pair: docker ; commit
   pair: docker ; context
   pair: docker ; cp
   pair: docker ; diff
   pair: docker ; exec
   pair: docker ; export
   pair: docker ; help
   pair: docker ; history
   pair: docker ; images
   pair: docker ; kill
   pair: docker ; logs
   pair: docker ; inspect
   pair: docker ; login
   pair: docker ; ps
   pair: docker ; pull
   pair: docker ; run
   pair: docker ; run  --detach-keys
   pair: docker ; rename
   pair: docker ; search
   pair: docker ; stop
   pair: docker ; tag
   pair: docker ; volume

.. _docker_commands:

==============================
Commands
==============================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/docker/#description





.. _docker_help:

docker help
=============

   - https://docs.docker.com/engine/reference/commandline/help/


::

    docker help


::

    Usage:  docker COMMAND

    A self-sufficient runtime for containers

    Options:
          --config string      Location of client config files (default "/home/pvergain/.docker")
      -D, --debug              Enable debug mode
      -H, --host list          Daemon socket(s) to connect to
      -l, --log-level string   Set the logging level ("debug"|"info"|"warn"|"error"|"fatal") (default "info")
          --tls                Use TLS; implied by --tlsverify
          --tlscacert string   Trust certs signed only by this CA (default "/home/pvergain/.docker/ca.pem")
          --tlscert string     Path to TLS certificate file (default "/home/pvergain/.docker/cert.pem")
          --tlskey string      Path to TLS key file (default "/home/pvergain/.docker/key.pem")
          --tlsverify          Use TLS and verify the remote
      -v, --version            Print version information and quit

    Management Commands:
      config      Manage Docker configs
      container   Manage containers
      image       Manage images
      network     Manage networks
      node        Manage Swarm nodes
      plugin      Manage plugins
      secret      Manage Docker secrets
      service     Manage services
      swarm       Manage Swarm
      system      Manage Docker
      trust       Manage trust on Docker images
      volume      Manage volumes

    Commands:
      attach      Attach local standard input, output, and error streams to a running container
      build       Build an image from a Dockerfile
      commit      Create a new image from a container's changes
      cp          Copy files/folders between a container and the local filesystem
      create      Create a new container
      diff        Inspect changes to files or directories on a container's filesystem
      events      Get real time events from the server
      exec        Run a command in a running container
      export      Export a container's filesystem as a tar archive
      history     Show the history of an image
      images      List images
      import      Import the contents from a tarball to create a filesystem image
      info        Display system-wide information
      inspect     Return low-level information on Docker objects
      kill        Kill one or more running containers
      load        Load an image from a tar archive or STDIN
      login       Log in to a Docker registry
      logout      Log out from a Docker registry
      logs        Fetch the logs of a container
      pause       Pause all processes within one or more containers
      port        List port mappings or a specific mapping for the container
      ps          List containers
      pull        Pull an image or a repository from a registry
      push        Push an image or a repository to a registry
      rename      Rename a container
      restart     Restart one or more containers
      rm          Remove one or more containers
      rmi         Remove one or more images
      run         Run a command in a new container
      save        Save one or more images to a tar archive (streamed to STDOUT by default)
      search      Search the Docker Hub for images
      start       Start one or more stopped containers
      stats       Display a live stream of container(s) resource usage statistics
      stop        Stop one or more running containers
      tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
      top         Display the running processes of a container
      unpause     Unpause all processes within one or more containers
      update      Update configuration of one or more containers
      version     Show the Docker version information
      wait        Block until one or more containers stop, then print their exit codes

    Run 'docker COMMAND --help' for more information on a command.



.. _docker_attach:

docker attach
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/attach/
   - :ref:`view_ids`



.. _docker_build:

docker build
======================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/build/
   - https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#build-context

Description
------------

The docker build command builds Docker images from a **Dockerfile**
and a context_. A build’s context is the set of files located in the
specified PATH or URL. The build process can refer to any of the files
in the context.
For example, your build can use a COPY instruction to reference a file
in the context.
The URL parameter can refer to three kinds of resources:

- Git repositories,
- pre-packaged tarball contexts
- and plain text files.


::

    docker build --tag gdevops/mydjango4 .


.. _context:  https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#build-context


docker build --no-cache
-------------------------

You can force a rebuild with docker build --no-cache ....


.. _docker_commit:

docker commit
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/commit/
   - :ref:`commit_new_image`
   - :ref:`commit_crashed`



.. _docker_context:

docker context (NEW docker 19.03)
==================================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/context/
   - https://newsletter.bretfisher.com/issues/new-docker-feature-change-your-context-203100



.. _docker_cp:

docker cp
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/cp/
   - :ref:`commit_new_image`



.. _docker_diff:

docker diff
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/diff/
   - :ref:`inspect_the_changes`
   - :ref:`viewing_system_changes`


.. _docker_exec:

docker exec
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/exec/
   - :ref:`go_inside_container`


Examples::

    docker run -d -p 8000:5000 -p 8001:5001 --name myany test_sqlanywhere:latest
    docker exec -ti myany bash


.. _docker_export:

docker export
==============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/export/
   - :ref:`dump_container`



.. _docker_history:

docker history
================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/history/
   - :ref:`viewing_history`


::

    docker history gdevops/mydjango4



.. _docker_inspect:

docker inspect
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/inspect/
   - :ref:`naming_containers`
   - :ref:`inspecting_container`
   - :ref:`inspect_format`


.. _docker_images:

docker images
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/images/
   - :ref:`showing_images`


::

    docker images


::

    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    gdevops/mydjango4   latest              105b33d63fe8        5 minutes ago       984MB
    python              3.6                 29d2f3226daf        3 weeks ago         911MB



.. _docker_kill:

docker kill
=============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/kill/
   - :ref:`container_stop`


.. _docker_login:

docker login
================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/login/
   - :ref:`login_dockerhub`


.. _docker_logs:

docker logs
=============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/logs/
   - :ref:`view_logs`


Description
--------------

Fetch the logs of a container.

Usage::

    docker logs [OPTIONS] CONTAINER


::

    docker logs apache



.. _docker_ps:

docker ps
===========

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/ps/
   - :ref:`list_running_containers`
   - :ref:`view_ids`
   - :ref:`docker_ps_filter`


docker ps --filter
---------------------

.. seealso::

   - :ref:`docker_ps_filter`


.. _docker_pull:

docker pull
===========

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/pull/
   - :ref:`downloading_images`



.. _docker_rename:

docker rename
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/rename/
   - :ref:`renaming_container`



.. _docker_rm:

docker rm
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/rm/


Description
--------------------

Remove one or more containers


Usage
-----

::

    docker rm [OPTIONS] CONTAINER [CONTAINER...]


Examples::

    docker rm $(docker ps -aq)



.. _docker_rmi:

docker rmi
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/rmi/


Examples::

    docker rmi $(docker images --filter "dangling=true" -q)


Description
---------------

Remove one or more images

Usage
--------

::

    docker rmi [OPTIONS] IMAGE [IMAGE...]


.. _docker_run:

docker run
===========

- https://docs.docker.com/engine/reference/commandline/run/
- :ref:`avril_2018`
- :ref:`specify_name`
- :ref:`docker_run_labels`
- :ref:`container_detach_keys`
- :ref:`commit_crashed`


detach-keys
-------------

.. seealso::

   - :ref:`container_detach_keys`


downloading images
---------------------

.. seealso::

   - :ref:`downloading_images`

labels
---------

.. seealso::

   - :ref:`docker_run_labels`


entrypoint
-----------

.. seealso::

   - :ref:`commit_crashed`


.. _docker_search:

docker search
================

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/search/


Description
--------------

Search the Docker Hub for images.

Usage::

    docker search [OPTIONS] TERM


::

    docker search apache


::

    NAME                                           DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
    tomcat                                         Apache Tomcat is an open source implementati…   2063                [OK]
    httpd                                          The Apache HTTP Server Project                  2038                [OK]
    cassandra                                      Apache Cassandra is an open-source distribut…   868                 [OK]
    maven                                          Apache Maven is a software project managemen…   698                 [OK]
    solr                                           Solr is the popular, blazing-fast, open sour…   586                 [OK]
    zookeeper                                      Apache ZooKeeper is an open-source server wh…   484                 [OK]
    eboraas/apache-php                             PHP5 on Apache (with SSL support), built on …   139                                     [OK]
    eboraas/apache                                 Apache (with SSL support), built on Debian      90                                      [OK]
    webdevops/php-apache-dev                       PHP with Apache for Development (eg. with xd…   78                                      [OK]
    webdevops/php-apache                           Apache with PHP-FPM (based on webdevops/php)    74                                      [OK]
    groovy                                         Apache Groovy is a multi-faceted language fo…   58                  [OK]
    tomee                                          Apache TomEE is an all-Apache Java EE certif…   56                  [OK]
    nimmis/apache-php5                             This is docker images of Ubuntu 14.04 LTS wi…   53                                      [OK]
    apacheignite/ignite                            Apache Ignite In-Memory docker image.           44                                      [OK]
    bitnami/apache                                 Bitnami Apache Docker Image                     42                                      [OK]
    linuxserver/apache                             An Apache container, brought to you by Linux…   18
    apache/nutch                                   Apache Nutch                                    15                                      [OK]
    1and1internet/ubuntu-16-apache-php-7.0         ubuntu-16-apache-php-7.0                        13                                      [OK]
    webdevops/apache                               Apache container                                11                                      [OK]
    antage/apache2-php5                            Docker image for running Apache 2.x with PHP…   10                                      [OK]
    lephare/apache                                 Apache container                                4                                       [OK]
    newdeveloper/apache-php                        apache-php7.2                                   3
    mastertinner/apache-directory-index-resource   A Concourse resource for the apache director…   1                                       [OK]
    secoresearch/apache-varnish                    Apache+PHP+Varnish5.0                           0                                       [OK]
    jelastic/apachephp                             An image of the Apache PHP application serve…   0




docker stack
==============


.. toctree::
   :maxdepth: 3

   docker_stack/docker_stack



docker swarm
==============

.. toctree::
   :maxdepth: 3

   docker_swarm/docker_swarm


.. _docker_system:

docker system
================

- https://docs.docker.com/engine/reference/commandline/system/


.. _docker_system_prune:

docker system  prune
------------------------

::

    docker systemp prune



.. _docker_stop:

docker stop
===========

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/stop/
   - :ref:`container_stop`


.. _docker_tag:

docker tag
===========

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/tag/
   - :ref:`tagging_images`


.. _docker_volume:

docker volume
===============

.. seealso::

   - https://docs.docker.com/engine/reference/commandline/volume/


::

    $ docker help volume

::

    Usage:  docker volume COMMAND

    Manage volumes

    Options:


    Commands:
      create      Create a volume
      inspect     Display detailed information on one or more volumes
      ls          List volumes
      prune       Remove all unused local volumes
      rm          Remove one or more volumes

    Run 'docker volume COMMAND --help' for more information on a command.
