

.. index::
   pair: Stéphane ; Beuret
   pair: Docker ; Stéphane Beuret
   pair: Kubernetes ; Stéphane Beuret


.. _stephane_beuret:

==============================
Stéphane Beuret
==============================

.. seealso::

   - https://x.com/Saphoooo
   - https://connect.ed-diamond.com/auteur/view/73156-beuret_stephane
   - https://github.com/de13
   - https://www.meetup.com/fr-FR/Luxembourg-Rancher-Meetup/members/216544162/




Activités 2018
=======================================


GNU/Linux Mag hors série N°98 septembre 2018
------------------------------------------------


.. seealso::

   - :ref:`linuxmag_hs98_2018`
   - https://boutique.ed-diamond.com/en-kiosque/1356-gnulinux-magazine-hs-98.html



GNU/linux mag N°217 juillet 2018
--------------------------------

.. seealso::

   - https://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-217/Vous-avez-dit-event-driven



GNU/linux mag N°214 avril 2018
--------------------------------

.. seealso::

   - https://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-214/Stockage-persistant-dans-Kubernetes-avec-Rook




GNU/linux mag N°211 janvier 2018
--------------------------------

.. seealso::

   - https://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-211/Introduction-Serverless-et-Function-as-a-Service-FaaS


GNU/linux mag N°204 mai 2017
--------------------------------


.. seealso::

   - https://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-204/Deployez-Kubernetes-sur-vos-Raspberry-Pi-avec-Kubeadm
