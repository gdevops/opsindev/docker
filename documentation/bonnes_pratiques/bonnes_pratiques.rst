
.. index::
   pair: Bonnes pratiques ; Docker
   pair: Best practices ; Dockerfiles


.. _bonnes_pratiques_docker:

===========================
Bonnes pratiques Docker
===========================

.. contents::
   :depth: 5


actu.alfa-safety.fr
======================

.. seealso::

   - https://actu.alfa-safety.fr/devops/docker-en-production/


Docker est largement utilisé en développement mais bien souvent les
choses se compliquent en production:

- d’abord l’application ne fonctionne pas toujours correctement en prod,
- les performances ne suivent pas,
- ensuite on s’aperçoit que l’on a oublié de prendre en compte un
  certain nombre d’éléments indispensables en production: monitoring,
  scalabilité, contraintes réseaux.

La facilité est alors de dire : Docker fonctionne bien en Dev mais n’est
pas un outil adapté à la production.
**Bien au contraire**, Docker en production doit permettre de faciliter et
sécuriser les déploiements tout en rendant votre application scalable.

Mais pour cela, il faut bien fonctionner en mode Devops et respecter un
certain nombre de bonnes pratiques. C’est en tant que telle une
compétence ou expertise Docker en production qu’il faut développer.

Enfin quand votre production atteint une certaine complexité, et que le
nombre de conteneurs que vous gérez se compte en dizaines, il faut
envisager de passer sur un orchestrateur de conteneurs.

Avant d’attaquer le vif du sujet, vous pouvez revenir sur notre
précédent article sur les bases de Docker.


Best practices for writing Dockerfiles
========================================

.. seealso::

   - https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/
   - https://docs.docker.com/engine/reference/builder/


Docker can build images automatically by reading the instructions from a
Dockerfile, a text file that contains all the commands, in order, needed
to build a given image. Dockerfiles adhere to a specific format and use
a specific set of instructions.

You can learn the basics on the `Dockerfile Reference page`_. If you’re new
to writing Dockerfiles, you should start there.

This document covers the best practices and methods recommended by
Docker, Inc. and the Docker community for building efficient images.

To see many of these practices and recommendations in action, check out
the Dockerfile for buildpack-deps.

.. note:: for more detailed explanations of any of the Dockerfile
   commands mentioned here, visit the Dockerfile Reference page.



.. _`Dockerfile Reference page`:  https://docs.docker.com/engine/reference/builder/



.. toctree::
   :maxdepth: 3

   9_piliers/9_piliers


Best practices for writing Dockerfiles from Nick Janetakis
=============================================================

.. toctree::
   :maxdepth: 3

   best_practices/best_practices
