
.. index::
   pair: Tutoriels ; Get Started


.. _get_started:

======================================================
Get started (https://docs.docker.com/get-started/)
======================================================

.. seealso::

   - https://docs.docker.com/get-started/
   - :ref:`tutos_docker_windows`


.. contents::
   :depth: 4


docker run hello-world
=======================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>docker run hello-world

::

	Hello from Docker!
	This message shows that your installation appears to be working correctly.

	To generate this message, Docker took the following steps:
	 1. The Docker client contacted the Docker daemon.
	 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
		(amd64)
	 3. The Docker daemon created a new container from that image which runs the
		executable that produces the output you are currently reading.
	 4. The Docker daemon streamed that output to the Docker client, which sent it
		to your terminal.

	To try something more ambitious, you can run an Ubuntu container with:
	 $ docker run -it ubuntu bash

	Share images, automate workflows, and more with a free Docker ID:
	 https://cloud.docker.com/

	For more examples and ideas, visit:
	 https://docs.docker.com/engine/userguide/


docker --version
==================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>docker --version

::

    Docker version 17.12.0-ce, build c97c6d6


Conclusion
=============

The unit of scale being an individual, portable executable has vast
implications.

It means CI/CD can push updates to any part of a distributed application,
system dependencies are not an issue, and resource density is increased.

Orchestration of scaling behavior is a matter of spinning up new
executables, not new VM hosts.

We’ll be learning about all of these things, but first let’s learn to walk.




Parts
=======

.. toctree::
   :maxdepth: 3

   part2/part2
   part3/part3
   part4/part4
