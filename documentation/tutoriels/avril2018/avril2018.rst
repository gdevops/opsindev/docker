

.. index::
   pair: Tutoriel ; Avril 2018
   pair: Jérôme Petazzoni ; Avril 2018


.. _avril_2018:

======================================================
Avril 2018 container training from Jérôme Petazzoni
======================================================

.. seealso::

   - https://avril2018.container.training
   - :ref:`petazzoni`
   - https://github.com/jpetazzo/container.training/graphs/contributors
   - https://github.com/jpetazzo/container.training


.. toctree::
   :maxdepth: 6

   intro/intro
   chapter1/chapter1
   chapter2/chapter2
   chapter3/chapter3
   chapter4/chapter4
   chapter5/chapter5
   chapter6/chapter6
   chapter7/chapter7
   chapter8/chapter8
