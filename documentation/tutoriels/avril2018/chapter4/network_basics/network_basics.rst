

.. index::
   pair: Networking basics ; Container


.. _container_network_basics:

====================================================
Container networking basics
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#265
   - https://avril2018.container.training/intro.yml.html#10
   - :ref:`petazzoni`
