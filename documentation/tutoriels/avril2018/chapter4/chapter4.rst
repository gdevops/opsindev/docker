

.. index::
   pair: Naming ; Containers


.. _chapter4_avril_2018:

====================================================
Chapter4 Avril 2018
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#231
   - https://avril2018.container.training/intro.yml.html#9
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4

   naming/naming
   labels/labels
   inside_container/inside_container
   network_basics/network_basics
   network_drivers/network_drivers
   network_model/network_model
   discovery/discovery
   ambassadors/ambassadors
