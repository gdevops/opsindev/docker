
===========
README.txt
===========

Export/Import
==============

Export
-------

- pg_dump -U postgres --clean --create -f db.dump.sql db_id3_intranet

L'option *create* produit la ligne suivante sous Windows::

    CREATE DATABASE db_id3_intranet WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';


Il faut la remplacer par la ligne suivante sous GNU/Linux::

    CREATE DATABASE db_id3_save WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';



Import
-------

- psql -U postgres -f db.dump.sql


TIP: on peut modifier l'entête du fichier texte db.dump.sql
-------------------------------------------------------------

On peut changer le nom de la base de données par exemple.


::

	--
	-- PostgreSQL database dump
	--

	-- Dumped from database version 10.1
	-- Dumped by pg_dump version 10.1

	SET statement_timeout = 0;
	SET lock_timeout = 0;
	SET idle_in_transaction_session_timeout = 0;
	SET client_encoding = 'UTF8';
	SET standard_conforming_strings = on;
	SET check_function_bodies = false;
	SET client_min_messages = warning;
	SET row_security = off;

	--
	-- Name: db_id3_save; Type: DATABASE; Schema: -; Owner: id3admin
	--

	CREATE DATABASE db_id3_save WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';


	ALTER DATABASE db_id3_save OWNER TO id3admin;

	\connect db_id3_save

	SET statement_timeout = 0;
	SET lock_timeout = 0;
	SET idle_in_transaction_session_timeout = 0;
	SET client_encoding = 'UTF8';
	SET standard_conforming_strings = on;
	SET check_function_bodies = false;
	SET client_min_messages = warning;
	SET row_security = off;

	--
	-- Name: db_id3_save; Type: COMMENT; Schema: -; Owner: id3admin
	--

	COMMENT ON DATABASE db_id3_save IS 'La base db_id3_save';



db_id3_save=# \l
                                   Liste des bases de données
     Nom     | Propriétaire | Encodage | Collationnement | Type caract. |    Droits d'accès
-------------+--------------+----------+-----------------+--------------+-----------------------
 db_id3_save | id3admin     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
 postgres    | postgres     | UTF8     | en_US.utf8      | en_US.utf8   |
 template0   | postgres     | UTF8     | en_US.utf8      | en_US.utf8   | =c/postgres          +
             |              |          |                 |              | postgres=CTc/postgres
 template1   | postgres     | UTF8     | en_US.utf8      | en_US.utf8   | =c/postgres          +
             |              |          |                 |              | postgres=CTc/postgres
(4 lignes)




Pour lancer le serveur PostgreSQL
==================================

::

    docker-compose up



Pour accéder au conteneur
===========================


1) docker-compose exec db bash
-------------------------------


::

    docker-compose exec db bash



2) docker ps + docker exec -ti caa4db30ee94 bash
--------------------------------------------------

::

    docker ps

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker ps

::

	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
	caa4db30ee94        postgres:10.1       "docker-entrypoint.s…"   19 hours ago        Up 34 minutes       5432/tcp            container_intranet


docker exec -ti caa4db30ee94 bash
-----------------------------------

::


	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker exec -ti caa4db30ee94 bash

::


	root@caa4db30ee94:/# ls -als
	total 80
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 .
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 ..
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:04 bin
	4 drwxr-xr-x   2 root root 4096 Nov 19 15:25 boot
	4 drwxr-xr-x   2 root root 4096 Jan 31 08:22 code
	0 drwxr-xr-x   5 root root  340 Jan 31 07:46 dev
	4 drwxr-xr-x   2 root root 4096 Dec 12 06:04 docker-entrypoint-initdb.d
	0 lrwxrwxrwx   1 root root   34 Dec 12 06:05 docker-entrypoint.sh -> usr/local/bin/docker-entrypoint.sh
	0 -rwxr-xr-x   1 root root    0 Jan 30 13:46 .dockerenv
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 etc
	4 drwxr-xr-x   2 root root 4096 Nov 19 15:25 home
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 lib
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 lib64
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 media
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 mnt
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 opt
	0 dr-xr-xr-x 132 root root    0 Jan 31 07:46 proc
	4 drwx------   1 root root 4096 Jan 30 14:32 root
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:05 run
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:04 sbin
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 srv
	0 dr-xr-xr-x  13 root root    0 Jan 31 07:46 sys
	4 drwxrwxrwt   1 root root 4096 Jan 30 13:46 tmp
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 usr
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 var


Mardi 30 janvier 2018
=======================


docker-compose.yml
---------------------


::

	version: "3"

	services:
	  db:
		image: postgres:10.1
		volumes:
		  - postgres_data:/var/lib/postgresql/data/

		environment:
		  POSTGRES_USER: id3admin
		  POSTGRES_PASSWORD: id338
		  POSTGRES_DB: db_id3_intranet

	volumes:
	  postgres_data:

docker-compose up
--------------------

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker-compose up

::

	WARNING: The Docker Engine you're using is running in swarm mode.

	Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

	To deploy your application across the swarm, use `docker stack deploy`.

	Pulling db (postgres:10.1)...
	10.1: Pulling from library/postgres
	723254a2c089: Downloading [====>                                              ]  4.128MB/45.12MB

	ba1542fb91f3: Download complete
	c7195e642388: Download complete
	95424deca6a2: Downloading [=====================>                             ]  2.785MB/6.577MB
	2d7d4b3a4ce2: Waiting
	fbde41d4a8cc: Waiting
	880120b92add: Waiting
	9a217c784089: Waiting
	d581543fe8e7: Waiting
	e5eff8940bb0: Waiting
	462d60a56b09: Waiting
	135fa6b9c139: Waiting


::

	Pulling db (postgres:10.1)...
	10.1: Pulling from library/postgres
	723254a2c089: Pull complete
	39ec0e6c372c: Pull complete
	ba1542fb91f3: Pull complete
	c7195e642388: Pull complete
	95424deca6a2: Pull complete
	2d7d4b3a4ce2: Pull complete
	fbde41d4a8cc: Pull complete
	880120b92add: Pull complete
	9a217c784089: Pull complete
	d581543fe8e7: Pull complete
	e5eff8940bb0: Pull complete
	462d60a56b09: Pull complete
	135fa6b9c139: Pull complete
	Digest: sha256:3f4441460029e12905a5d447a3549ae2ac13323d045391b0cb0cf8b48ea17463
	Status: Downloaded newer image for postgres:10.1
	Creating postgresql_db_1 ... done
	Attaching to postgresql_db_1
	db_1  | 2018-01-30 10:40:10.756 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
	db_1  | 2018-01-30 10:40:10.756 UTC [1] LOG:  listening on IPv6 address "::", port 5432
	db_1  | 2018-01-30 10:40:10.840 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
	db_1  | 2018-01-30 10:40:10.999 UTC [23] LOG:  database system was interrupted; last known up at 2018-01-22 12:28:43 UTC
	db_1  | 2018-01-30 10:40:11.377 UTC [23] LOG:  database system was not properly shut down; automatic recovery in progress
	db_1  | 2018-01-30 10:40:11.430 UTC [23] LOG:  redo starts at 0/1633958
	db_1  | 2018-01-30 10:40:11.430 UTC [23] LOG:  invalid record length at 0/1633A38: wanted 24, got 0
	db_1  | 2018-01-30 10:40:11.430 UTC [23] LOG:  redo done at 0/1633A00
	db_1  | 2018-01-30 10:40:11.788 UTC [1] LOG:  database system is ready to accept connections


::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker exec -it 02b2487f304e bash

::

	root@02b2487f304e:/# psql -U postgres
	psql (10.1)
	Type "help" for help.

	postgres=# \dt
	Did not find any relations.
	postgres=# \l
										List of databases
		  Name       |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
	-----------------+----------+----------+------------+------------+-----------------------
	 db_id3_intranet | id3admin | UTF8     | en_US.utf8 | en_US.utf8 |
	 postgres        | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
	 template0       | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
					 |          |          |            |            | postgres=CTc/postgres
	 template1       | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
					 |          |          |            |            | postgres=CTc/postgres
	(4 rows)
