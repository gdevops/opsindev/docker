

.. index::
   pair: Dockerfile  ; docs (Funkwhale)


.. _funkwhale_doc_docker:

===========================================
Funkwhale docs Dockerfile
===========================================





Dockerfile
============

.. literalinclude:: Dockerfile
   :linenos:


conf.py
============

.. literalinclude:: conf.py
   :linenos:


build_docs.sh
===============

.. literalinclude:: build_docs.sh
   :linenos:


build_swagger.sh
=================

.. literalinclude:: build_swagger.sh
   :linenos:
