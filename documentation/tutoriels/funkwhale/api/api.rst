

.. index::
   pair: Dockerfile  ; api (Funkwhale)


.. _funkwhale_api_docker:

===========================================
Funkwhale api Dockerfile
===========================================





Dockerfile
============

.. literalinclude:: Dockerfile
   :linenos:


compose/django/daphne.sh
==========================

.. literalinclude:: compose/django/daphne.sh
   :linenos:


compose/django/dev-entrypoint.sh
===================================

.. literalinclude:: compose/django/dev-entrypoint.sh
   :linenos:


compose/django/dev-entrypoint.sh
===================================

.. literalinclude:: compose/django/entrypoint.sh
   :linenos:
