
.. index::
   pair: Mickael Baron ; Tutoriaux
   pair: docker ; ROS

.. _mickael_baron_tuto:

============================================================================================
Tutoriel pour préparer son environnement de développement ROS avec Docker de Mickael Baron
============================================================================================

.. seealso::

   - https://mbaron.developpez.com/
   - https://x.com/mickaelbaron
   - http://www.ros.org/
   - https://hub.docker.com/_/ros/
   - https://github.com/osrf/docker_images
   - https://mbaron.developpez.com/tutoriels/ros/environnement-developpement-ros-docker/
   - https://www.developpez.net/forums/d1857234/general-developpement/programmation-systeme/embarque/tutoriel-preparer-environnement-developpement-ros-docker/



.. figure:: mickael_baron.png
   :align: center

   Mickael Baron





Format PDF
===========

.. seealso:

   - http://mbaron.developpez.com/tutoriels/ros/environnement-developpement-ros-docker/mickaelbaron-rosdocker.pdf



Introduction
==============


Ce tutoriel s'intéresse à présenter ROS (Robot Operating System) et à
décrire comment proposer à un développeur un environnement de
développement prêt à l'emploi quel que soit le système d'exploitation
utilisé pour le développement et pour le déploiement.

En effet, par défaut, ROS n'est disponible que sous un système Linux.
Il existe bien des versions pour macOS, mais elles sont expérimentales
et il n'existe aucune version pour Windows.
Par ailleurs, même si nous souhaitions utiliser ces versions expérimentales,
aurions-nous le même comportement une fois notre programme déployé ?
Bien entendu, si du matériel doit être utilisé spécifiquement (bras robotisé,
carte Raspberry…) pour un système d'exploitation donné (généralement
sous Linux), nous n'aurions pas le choix d'utiliser le système en question.




Conclusion
===========

Ce tutoriel a montré comment utiliser Docker pour le développement des
applications basées sur ROS (Robot Operating System).

Plus précisément avec Docker, nous avons vu :

- comment enrichir l'image ROS ;
- comment utiliser les outils fournis par ROS (rostopic) en exécutant
  un conteneur ;
- comment exécuter une application ROS de plusieurs nœuds en créant
  plusieurs conteneurs « à la main » ou via l'outil d'orchestration
  docker-compose ;
- comment démarrer des nœuds qui possèdent des interfaces graphiques
  via le déport d'affichage (serveur X11) ;
- comment déployer une application ROS sur plusieurs machines physiques
  via l'utilisation de docker-machine ;
- comment autoriser un conteneur à accéder aux éléments matériels
  du système hôte ;
- comment synchroniser son répertoire de travail sur plusieurs machines.

De nombreuses perspectives sont à explorer :

- faciliter l'usage des lignes de commandes Docker via l'utilisation
  d'alias spécifiques. Nous pourrions aller plus loin en masquant la
  complexité de Docker dans un environnement de développement préconfiguré
  et plus simple en réduisant la longueur des lignes de commandes ;
- effectuer de la cross-compilation pour des programmes ROS développés
  en C++. Nous avons choisi la simplicité avec Python, mais dans le cas
  de C++ il y a une compilation à réaliser en amont. Qu'en est-il lorsqu'il
  faut compiler sur une plateforme matérielle différente X86 vs ARM ;
- utiliser Swarm de la famille Docker pour la création d'un cluster ;
- gérer le problème de la redondance de nœuds par Docker.

Dans le prochain tutoriel consacré à ROS, nous nous intéresserons à la
possibilité de développer des programmes ROS avec le langage Java.
Nous montrerons que ROS une architecture où plusieurs programmes
développés dans des langages différents peuvent communiquer.
À ce titre, nous ferons communiquer des nœuds écrits dans des langages
différents tels que Python et Java.
