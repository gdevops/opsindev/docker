

.. index::
   pair: Images ; Gitlab


.. _images_gitlab:

=====================================
Images **Gitlab community edition**
=====================================


.. seealso::

   - https://store.docker.com/images/gitlab-community-edition
   - https://about.gitlab.com/features/




.. figure:: gitlab-logo-large.png
   :align: center

   Le logo Gitlab


Short Description
==================

GitLab includes Git repository management, issue tracking, code review,
an IDE, activity streams, wikis, and more.

Open source collaboration and source control management: code, test,
and deploy together! More details on features can be found on https://about.gitlab.com/features/
