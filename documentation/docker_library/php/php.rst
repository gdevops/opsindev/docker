

.. index::
   pair: Images ; PHP


.. _images_php:

=====================================
Images **PHP**
=====================================


.. seealso::

   - https://store.docker.com/images/php
   - https://en.wikipedia.org/wiki/PHP




.. figure:: php_logo.png
   :align: center

   Le logo PHP


Short Description
==================

While designed for web development, the PHP scripting language also
provides general-purpose use.


What is PHP ?
===============

.. seealso::

   - https://en.wikipedia.org/wiki/PHP

PHP is a server-side scripting language designed for web development,
but which can also be used as a general-purpose programming language.
PHP can be added to straight HTML or it can be used with a variety of
templating engines and web frameworks.

PHP code is usually processed by an interpreter, which is either
implemented as a native module on the web-server or as a
common gateway interface (CGI).
