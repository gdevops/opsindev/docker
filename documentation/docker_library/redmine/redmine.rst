

.. index::
   pair: Images ; Redmine


.. _images_redmine:

=====================================
Images **Redmine**
=====================================


.. seealso::

   - https://store.docker.com/images/redmine




.. figure:: redmine_logo.png
   :align: center

   Le logo redmine


Short Description
==================

Redmine is a flexible project management web application written using
Ruby on Rails framework.
