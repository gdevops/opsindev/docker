

.. index::
   pair: Image ; pipenv


.. _image_pipenv:

==============================
Images **pipenv**
==============================


.. seealso::

   - https://hub.docker.com/r/kennethreitz/pipenv/




Short Description
==================

Pipenv Docker image.


What is Python ?
==================

Pipenv is a tool that aims to bring the best of all packaging worlds
(bundler, composer, npm, cargo, yarn, etc.) to the Python world.
Windows is a first--class citizen, in our world.

It automatically creates and manages a virtualenv for your projects, as
well as adds/removes packages from your Pipfile as you
install/uninstall packages. It also generates the ever--important
Pipfile.lock, which is used to produce deterministic builds.


Dockerfile
============

.. seealso::

   - https://github.com/pypa/pipenv/blob/master/Dockerfile

::

    FROM heroku/heroku:18-build

    ENV DEBIAN_FRONTEND noninteractive
    ENV LC_ALL C.UTF-8
    ENV LANG C.UTF-8

    # -- Install Pipenv:
    RUN apt update && apt upgrade -y && apt install python3.7-dev -y
    RUN curl --silent https://bootstrap.pypa.io/get-pip.py | python3.7

    # Backwards compatility.
    RUN rm -fr /usr/bin/python3 && ln /usr/bin/python3.7 /usr/bin/python3

    RUN pip3 install pipenv

    # -- Install Application into container:
    RUN set -ex && mkdir /app

    WORKDIR /app

    # -- Adding Pipfiles
    ONBUILD COPY Pipfile Pipfile
    ONBUILD COPY Pipfile.lock Pipfile.lock

    # -- Install dependencies:
    ONBUILD RUN set -ex && pipenv install --deploy --system

    # --------------------
    # - Using This File: -
    # --------------------

    # FROM kennethreitz/pipenv

    # COPY . /app

    # -- Replace with the correct path to your app's main executable
    # CMD python3 main.py



How to use this image
======================

.. seealso::

   - http://python-responder.org/en/latest/deployment.html#docker-deployment

.. code-block:: bash

    from kennethreitz/pipenv

    COPY . /app
    CMD python3 api.py
