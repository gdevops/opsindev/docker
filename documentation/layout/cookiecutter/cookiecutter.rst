
.. index::
   ! cookiecutter

.. _docker_cookiecutter:

==================================
cookiecutter docker
==================================

.. seealso::

   - :ref:`physical_architecture`

.. toctree::
   :maxdepth: 6

   django/django
