
.. index::
   pair: Qui utilise ; Docker
   pair: Paypal ; Docker


.. _qui_docker:

=====================================
Qui utilise Docker en production ?
=====================================


.. seealso::

   - https://actu.alfa-safety.fr/devops/docker-en-production/






Historique
===========

Janvier 2018
-------------

.. seealso::

   - https://blog.docker.com/2018/01/5-tips-learn-docker-2018/

As the holiday season ends, many of us are making New Year’s resolutions
for 2018. Now is a great time to think about the new skills or
technologies you’d like to learn.
So much can change each year as technology progresses and companies are
looking to innovate or modernize their legacy applications or infrastructure.

At the same time the market for Docker jobs continues to grow as
companies such as Visa, MetLife and Splunk adopt Docker Enterprise
Edition ( EE) in production.



Paypal
======


.. seealso::

   - https://www.docker.com/customers/paypal-uses-docker-containerize-existing-apps-save-money-and-boost-security


Challenges
-----------

Today PayPal is leveraging OpenStack for their private cloud and runs
more than 100,000 VMs.
This private cloud runs 100% of their web and mid-tier applications
and services.
One of the biggest desires of the PayPal business is to modernize their
datacenter infrastructure, making it more on demand, improving its
security, meeting compliance regulations and lastly, making everything
cost efficient.

They wanted to refactor their existing Java and C++ legacy applications
by dockerizing them and deploying them as containers.

This called for a technology that provides a distributed application
deployment architecture and can manage workloads but must also be
deployed in both private, and eventually public cloud environments.
Being cost efficient was extremely important for the company.
Since PayPal runs their own cloud, they pay close attention to how much
money they are spending on actually running their datacenter infrastructure.

Functioning within the online payment industry, PayPal must ensure the
security of their internal data (binaries and artifacts with the source
code of their applications).
This makes them a **very security-conscious company**.

Their sensitive data needs to be kept on-premises where their security
teams can run ongoing scans and sign their code before deploying out
to production.
PayPal’s massive popularity is a good thing, but it also means they
must handle the deluge of demands from their users.
At times they process more than 200 payments per second.
When including Braintree and Venmo, the companies that PayPal acquired,
that number continues to soar even higher.
Recently, it was announced that Braintree is processing more than a
billion a month when it comes to mobile payments!.
That adds quite a bit of extra pressure on their infrastructure.


Solution
---------

Today PayPal uses Docker’s commercial solutions to enable them to not
only provide gains for their developers, in terms of productivity and
agility, but also for their infrastructure teams in the form of cost
efficiency and enterprise-grade security.

The tools being used in production today include:

- Docker Commercially Supported engine (CS Engine),
- Docker Trusted Registry
- as well as Docker Compose.

The company believes that containers and VMs can coexist and combine
the two technologies.
Leveraging Docker containers and VMs together gives PayPal the ability
to run more applications while reducing the number of total VMs,
optimizing their infrastructure.
This also allows PayPal to spin up a new application much more quickly,
and on an “as needed” basis.

Since containers are more lightweight and instantiate in a fraction
of a second, while VMs take minutes, they can roll out a new application
instance quickly, patch an existing application, or even add capacity
for holiday readiness to compensate for peak times within the year.

This helps drive innovation and help them outpace competition.
Docker Trusted Registry gives their team enterprise security features
like granular role based access controls, and image signing that
ensures that all of PayPal’s checks and balances are in place.

The tool provides them with the on-premises enterprise-grade registry
service they need in order to provide secure collaboration for their
image content. There security team can run ongoing scans and sign code
before deploying to production.

With Docker, the company has gained the ability to scale quickly,
deploy faster, and one day even provide local desktop-based development
environments with Docker. For that, they are looking to Docker for
Mac and Docker for Windows, which offer Docker as a local development
environment to their 4,000+ developers located across the globe.
