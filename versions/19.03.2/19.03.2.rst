

.. index::
   pair: docker ; 19.03.2 (2019-08-29)

.. _docker_19_03_2:

=================================
19.03.2 (2019-08-29)
=================================

.. seealso::

   - https://github.com/docker/docker-ce/tree/v19.03.2
   - https://github.com/docker/docker-ce/blob/v19.03.2/CHANGELOG.md
