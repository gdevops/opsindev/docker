.. Tutoriel Docker documentation master file, created by
   sphinx-quickstart on Fri Jan 12 10:55:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. figure:: images/Docker_logo.svg.png
   :align: center

   Logo Docker https://www.docker.com


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>
   
   
   
|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/docker/rss.xml>`_

.. _tuto_docker:

================
Opsindev Docker
================

- https://github.com/moby/moby
- https://fr.wikipedia.org/wiki/Docker_%28logiciel%29
- https://container.training/
- https://docs.docker.com/
- http://training.play-with-docker.com/
- https://avril2018.container.training/
- https://github.com/jpetazzo/container.training
- https://jpetazzo.github.io/2018/03/28/containers-par-ou-commencer/


.. toctree::
   :maxdepth: 5

   news/news
   installation/installation
   commands/commands
   documentation/documentation
   docker_swarm/docker_swarm
   docker_compose/docker_compose
   compose_file/compose_file
   glossaire/glossaire
   versions/versions

.. toctree::
   :maxdepth: 5

   meta/meta
