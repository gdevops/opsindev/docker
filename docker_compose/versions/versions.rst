.. index::
   pair: docker-compose; versions

.. _docker_compose_versions:

=========================
docker-compose versions
=========================

- https://github.com/docker/compose/releases
- https://docs.docker.com/compose/release-notes/

.. toctree::
   :maxdepth: 3


   2.16.0/2.16.0
   2.15.1/2.15.1
   2.3.3/2.3.3
   2.0.0/2.0.0
   1.27/1.27
   1.24
   1.22
   1.21
