.. index::
   pair: Docker ; Compose

.. _docker_compose:

==============================
**Docker compose**
==============================

- https://github.com/docker/compose/
- https://docs.docker.com/compose/
- https://jvns.ca/blog/2021/01/04/docker-compose-is-nice/
- https://docs.docker.com/compose/
- https://docs.docker.com/compose/samples-for-compose/
- https://www.aquasec.com/wiki/display/containers/Docker+Compose


Concepts clés
==============

Key concepts these samples cover

The samples should help you to:

- define services based on Docker images using Compose files
  docker-compose.yml and docker-stack.yml files
- understand the relationship between docker-compose.yml and Dockerfiles
- learn how to make calls to your application services from Compose files
- learn how to deploy applications and services to a swarm


Other definition
-----------------

Source: https://blog.sixeyed.com/windows-dockerfile-26/

You define applications in Compose in terms of services rather than
containers.
Services will be deployed as containers - but a service could be multiple
instances of a single container, so it's an abstraction beyond managing
containers on hosts.


Other links
=============

- https://www.aquasec.com/wiki/display/containers/Docker+Compose


heroku
--------


- https://devcenter.heroku.com/articles/local-development-with-docker-compose

awsome docker-compose
==========================

.. toctree::
   :maxdepth: 3

   awsome/awsome



docker-compose commands
==========================

.. toctree::
   :maxdepth: 3

   commands/commands


docker-compose V2
====================


.. toctree::
   :maxdepth: 3

   V2/V2


docker-compose for production
================================

.. toctree::
   :maxdepth: 3

   production/production


docker-compose TIPS
================================

.. toctree::
   :maxdepth: 3

   tips/tips



docker-compose versions
==========================

.. toctree::
   :maxdepth: 3

   versions/versions

Exemples
===========

.. toctree::
   :maxdepth: 3


   django/django
   gitlab_arm/gitlab_arm
