
.. index::
   pair: django ; compose


.. _compose_example_1:

=====================================
Compose file example 1
=====================================

.. seealso::

   - https://github.com/pydanny/cookiecutter-django/issues/1258




base.yml
===========

::

	version: '3.2'
	services:
	  postgres:
		build: ./compose/postgres
		environment:
		  - POSTGRES_USER_FILE=/run/secrets/pg_username
		  - POSTGRES_PASSWORD_FILE=/run/secrets/pg_password
		secrets:
		  - pg_username
		  - pg_password

	  django:
		command: /gunicorn.sh
		environment:
		  - USE_DOCKER=$DAPI_VAR:-yes
		  - DATABASE_URL=postgres://{username}:{password}@postgres:5432/{username}
		  - SECRETS_FILE=/run/secrets/django_s
		  - POSTGRES_USER_FILE=/run/secrets/pg_username
		  - POSTGRES_PASSWORD_FILE=/run/secrets/pg_password
		# My Deploy
		deploy:
			  replicas: 1
			  restart_policy:
				condition: on-failure
		secrets:
		  - pg_username
		  - pg_password
		  - django_s

	secrets:
		django_s:
			external: True
		pg_username:
			external: True
		pg_password:
			external: True


dev.yml
========

::

	version: '3.2'

	volumes:
	  postgres_data_dev: {}
	  postgres_backup_dev: {}

	services:
	  postgres:
		image: apple_postgres
		volumes:
		  - postgres_data_dev:/var/lib/postgresql/data
		  - postgres_backup_dev:/backups

	  django:
		image: apple_django
		build:
		  context: .
		  dockerfile: ./compose/django/Dockerfile-dev
		command: /start-dev.sh
		volumes:
		  - .:/app
		ports:
		  - "8000:8000"
		secrets:
		  - pg_username
		  - pg_password
		  - source: django_s
			#target: /app//.env

	  node:
		image: apple_node
		#user: $USER:-0
		build:
		  context: .
		  dockerfile: ./compose/node/Dockerfile-dev
		volumes:
		  - .:/app
		  - ${PWD}/gulpfile.js:/app/gulpfile.js
		  # http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html
		  - /app/node_modules
		  - /app/vendor
		command: "gulp"
		ports:
		  # BrowserSync port.
		  - "3000:3000"
		  # BrowserSync UI port.
		  - "3001:3001"
